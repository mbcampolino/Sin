package com.codecampos.sin.model

import android.content.Context
import android.content.res.Resources
import com.codecampos.sin.R
import com.codecampos.sin.model.database.CategoryModel
import java.util.*
import kotlin.collections.ArrayList

abstract class AppUtils {

    companion object AppUtils {

        fun getDayOfWeekName(dayOfWeek: Int, resources: Resources): String {
            return resources.getStringArray(R.array.days_of_week)[dayOfWeek - 1]
        }

        fun getMockedCategories(resources: Resources): ArrayList<CategoryModel> {

            val list: ArrayList<CategoryModel> = ArrayList()
            list.add(CategoryModel(0, resources.getString(R.string.food), true, 0))
            list.add(CategoryModel(0, resources.getString(R.string.health_care), true, 0))
            list.add(CategoryModel(0, resources.getString(R.string.`fun`), false, 0))
            list.add(CategoryModel(0, resources.getString(R.string.social), false, 0))
            list.add(CategoryModel(0, resources.getString(R.string.work), false, 0))

            return list
        }

        fun getDate(calendar: Calendar): String {

            /*é feito o plus(1) porque a classe calendar vai de 0 a 11*/

            var month: String = if (calendar.get(Calendar.MONTH) < 9) {
                "0${calendar.get(Calendar.MONTH).plus(1)}"
            } else {
                calendar.get(Calendar.MONTH).plus(1).toString()
            }

            var day: String = if (calendar.get(Calendar.DAY_OF_MONTH) < 10) {
                "0${calendar.get(Calendar.DAY_OF_MONTH)}"
            } else {
                calendar.get(Calendar.DAY_OF_MONTH).toString()
            }

            return "$day/$month/${calendar.get(Calendar.YEAR)}"
        }

        fun putPref(ctx: Context, value: String, boolean: Boolean) {
            val sharedPrefs = ctx.getSharedPreferences(ctx.getString(R.string.preferencekey), Context.MODE_PRIVATE)
            sharedPrefs.edit().putBoolean(value, boolean).apply()
        }

        fun getPref(ctx: Context, value: String, defaultReturn: Boolean = false): Boolean {
            val sharedPrefs = ctx.getSharedPreferences(ctx.getString(R.string.preferencekey), Context.MODE_PRIVATE)
            return sharedPrefs.getBoolean(value, defaultReturn)
        }

        fun getHello(ctx: Context): String {

            val calendar: Calendar = Calendar.getInstance()

            val hour = calendar.get(Calendar.HOUR_OF_DAY)

            if (hour in 3..12) {
                return ctx.getString(R.string.good_morning)
            }

            if (hour in 13..18) {
                return ctx.getString(R.string.good_afternoon)
            }

            if (hour in 0..2) {
                return ctx.getString(R.string.good_evening)
            }

            if (hour in 19..22) {
                return ctx.getString(R.string.good_evening)
            }

            return ""
        }

        infix fun Int.fdiv(i: Int): Double = this / i.toDouble()
    }

}