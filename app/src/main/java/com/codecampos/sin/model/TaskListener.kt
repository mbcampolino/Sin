package com.codecampos.sin.model

interface TaskListener {
    fun onUpdateTasks()
    fun callCategories(requestCode: Int = 100)
}