package com.codecampos.sin.model

class Constants {

    companion object Constants {

        val REPEAT_ONLY = 0
        val REPEAT_EVERYDAY = 1
        val REPEAT_DAY_OF_WEEK = 2
        val REPEAT_DAY_OF_MONTH = 3

        val ALL_CATEGORIES = 0
        val NEW_CATEGORY = 99999999
    }
}