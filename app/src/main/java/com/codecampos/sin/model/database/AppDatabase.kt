package com.codecampos.sin.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [(TaskModel::class), (DateTaskModel::class), (CategoryModel::class)], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun taskDataDao(): TaskDataDao
}