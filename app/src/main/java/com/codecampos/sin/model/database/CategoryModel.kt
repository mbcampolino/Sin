package com.codecampos.sin.model.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "categories")
data class CategoryModel(@PrimaryKey(autoGenerate = true) var id_category: Int,
                         @ColumnInfo(name = "name") var name: String,
                         @ColumnInfo(name = "checked") var checked: Boolean,
                         @ColumnInfo(name = "fixed") var fixed: Int) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id_category)
        parcel.writeString(name)
        parcel.writeByte(if (checked) 1 else 0)
        parcel.writeInt(fixed)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CategoryModel> {
        override fun createFromParcel(parcel: Parcel): CategoryModel {
            return CategoryModel(parcel)
        }

        override fun newArray(size: Int): Array<CategoryModel?> {
            return arrayOfNulls(size)
        }
    }
}