package com.codecampos.sin.model.database

import android.arch.persistence.room.*

@Dao
interface TaskDataDao {

    @Query("select * from dateoftasks join tasks on dateoftasks.id_task = tasks.id_task where dateoftasks.timestamp < :timestamp")
    fun getAllBeforeThatNow(timestamp: Long): List<TaskModelBeta>

    @Query("SELECT MAX(id_task) FROM tasks")
    fun getLastIdFromTask(): Int

    @Query("select * from dateoftasks join tasks on dateoftasks.id_task = tasks.id_task where dateoftasks.date = :date  ORDER BY dateoftasks.timestamp ASC")
    fun getFromDate(date: String): List<TaskModelBeta>

    @Query("select * from dateoftasks join tasks on dateoftasks.id_task = tasks.id_task where dateoftasks.date != :date and tasks.repeatType > 0 group by tasks.id_task")
    fun getEveryDayEvents(date: String): List<TaskModelBeta>

    @Query("SELECT COUNT(*) from tasks")
    fun countUsers(): Int

    @Query("SELECT * from categories")
    fun getCategories(): List<CategoryModel>

    @Query("SELECT * from categories where checked = 1 and id_category > :id")
    fun getCategoriesStatsBars(id: Int = 0): List<CategoryModel>

    @Query("SELECT COUNT(*) from dateoftasks where id_task = :id and date = :date")
    fun countTaskWithId(id: Int, date: String): Int

    @Query("SELECT COUNT(*) from tasks where fromCalendar = :id")
    fun countTaskWithCalendarId(id: Int): Int

    @Query("SELECT COUNT(*) from tasks where fromCalendar > 0")
    fun countTaskWithCalendar(): Int

    @Insert
    fun insert(task: TaskModel)

    @Insert
    fun insert(task: CategoryModel)

    @Insert
    fun insertAll(categories: List<CategoryModel>)

    @Insert
    fun insertDateOfTask(dateOfTask: DateTaskModel)

    @Update
    fun update(users: TaskModel)

    @Update
    fun updateDate(users: DateTaskModel)

    @Query("UPDATE dateoftasks SET checked = :checked WHERE id_date = :id")
    fun checkTask(checked: Boolean, id: Int)

    @Query("UPDATE categories SET checked = :checked WHERE id_category = :id")
    fun checkCategory(checked: Boolean, id: Int)

    @Query("UPDATE categories SET name = :name WHERE id_category = :id")
    fun changeCategoryName(name: String, id: Int)

    @Delete
    fun delete(user: TaskModel)

    @Delete
    fun delete(user: CategoryModel)

    @Query("delete from dateoftasks where id_task = :id_task")
    fun deleteDatesOfTask(id_task: Int)

    @Query("DELETE from tasks")
    fun deleteAll()

    @Query("UPDATE tasks SET type = 0 WHERE type = :id_category")
    fun updateTasksWithCategory(id_category: Int)

    @Query("delete from tasks where fromCalendar > 0")
    fun removeTasksWithCalendar()
}