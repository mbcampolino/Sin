package com.codecampos.sin.model.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "dateoftasks")
data class DateTaskModel(@PrimaryKey(autoGenerate = true) var id_date: Int,
                         @ColumnInfo(name = "date") var date: String,
                         @ColumnInfo(name = "timestamp") var timestamp: Long,
                         @ColumnInfo(name = "id_task") var idTask: Int,
                         @ColumnInfo(name = "checked") var checked: Boolean) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id_date)
        parcel.writeString(date)
        parcel.writeLong(timestamp)
        parcel.writeInt(idTask)
        parcel.writeByte(if (checked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DateTaskModel> {
        override fun createFromParcel(parcel: Parcel): DateTaskModel {
            return DateTaskModel(parcel)
        }

        override fun newArray(size: Int): Array<DateTaskModel?> {
            return arrayOfNulls(size)
        }
    }
}