package com.codecampos.sin.model

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.provider.CalendarContract.Instances
import com.codecampos.sin.model.database.TaskModel
import java.util.*
import kotlin.collections.ArrayList


class CalendarSync {

    class Events {
        companion object Events {

            val INSTANCE_PROJECTION = arrayOf(Instances.EVENT_ID,
                    Instances.BEGIN,
                    Instances.TITLE
            )

            // The indices for the projection array above.
            private val PROJECTION_ID_INDEX = 0
            private val PROJECTION_BEGIN_INDEX = 1
            private val PROJECTION_TITLE_INDEX = 2

            fun getEvents(context: Context, calendar: Calendar): ArrayList<TaskModel> {

                val eventsCalendar: ArrayList<TaskModel> = ArrayList()

                val cur: Cursor?
                val cr = context.contentResolver
                val builder = Instances.CONTENT_URI.buildUpon()

                val startTime = Calendar.getInstance()
                startTime.timeInMillis = calendar.timeInMillis
                val startMillis = startTime.timeInMillis

                ContentUris.appendId(builder, startMillis)
                ContentUris.appendId(builder, startMillis)

                cur = cr.query(builder.build(),
                        INSTANCE_PROJECTION,
                        null,
                        null,
                        null)

                while (cur.moveToNext()) {

                    val eventID = cur.getLong(PROJECTION_ID_INDEX)
                    val dateMillis = cur.getLong(PROJECTION_BEGIN_INDEX)
                    val title = cur.getString(PROJECTION_TITLE_INDEX)

                    val calendarEdit: Calendar = Calendar.getInstance()
                    calendarEdit.timeInMillis = dateMillis

                    eventsCalendar.add(TaskModel(0, title, 0, false, 0, calendarEdit.timeInMillis, eventID))
                }

                cur.close()

                return eventsCalendar
            }
        }
    }
}