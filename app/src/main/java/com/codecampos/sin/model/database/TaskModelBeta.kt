package com.codecampos.sin.model.database

import android.arch.persistence.room.ColumnInfo
import android.os.Parcel
import android.os.Parcelable

data class TaskModelBeta(@ColumnInfo(name = "name") var name: String,
                         @ColumnInfo(name = "type") var type: Int,
                         @ColumnInfo(name = "timestamp") var timestamp: Long,
                         @ColumnInfo(name = "hasHour") var hasHour: Boolean,
                         @ColumnInfo(name = "repeatType") var repeatType: Int,
                         @ColumnInfo(name = "date") var date: String,
                         @ColumnInfo(name = "id_task") var id_task: Int,
                         @ColumnInfo(name = "id_date") var id_date: Int,
                         @ColumnInfo(name = "checked") var checked: Boolean,
                         @ColumnInfo(name = "timeStampRoot") var timestampRoot: Long,
                         @ColumnInfo(name = "fromCalendar") var fromCalendar: Long) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readLong(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readLong(),
            parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(type)
        parcel.writeLong(timestamp)
        parcel.writeByte(if (hasHour) 1 else 0)
        parcel.writeInt(repeatType)
        parcel.writeString(date)
        parcel.writeInt(id_task)
        parcel.writeInt(id_date)
        parcel.writeByte(if (checked) 1 else 0)
        parcel.writeLong(timestampRoot)
        parcel.writeLong(fromCalendar)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskModelBeta> {
        override fun createFromParcel(parcel: Parcel): TaskModelBeta {
            return TaskModelBeta(parcel)
        }

        override fun newArray(size: Int): Array<TaskModelBeta?> {
            return arrayOfNulls(size)
        }
    }
}