package com.codecampos.sin.model.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "tasks")
data class TaskModel(@PrimaryKey(autoGenerate = true) var id_task: Int,
                     @ColumnInfo(name = "name") var name: String,
                     @ColumnInfo(name = "type") var type: Int,
                     @ColumnInfo(name = "hasHour") var hasHour: Boolean,
                     @ColumnInfo(name = "repeatType") var repeatType: Int,
                     @ColumnInfo(name = "timeStampRoot") var dateRoot: Long,
                     @ColumnInfo(name = "fromCalendar") var fromCalendar: Long) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readLong(),
            parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id_task)
        parcel.writeString(name)
        parcel.writeInt(type)
        parcel.writeByte(if (hasHour) 1 else 0)
        parcel.writeInt(repeatType)
        parcel.writeLong(dateRoot)
        parcel.writeLong(fromCalendar)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskModel> {
        override fun createFromParcel(parcel: Parcel): TaskModel {
            return TaskModel(parcel)
        }

        override fun newArray(size: Int): Array<TaskModel?> {
            return arrayOfNulls(size)
        }
    }
}