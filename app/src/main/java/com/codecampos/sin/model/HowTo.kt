package com.codecampos.sin.model

import android.app.Activity
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.codecampos.sin.R
import devs.mulham.horizontalcalendar.HorizontalCalendar
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetSequence
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.RectanglePromptBackground
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal


class HowTo {

    companion object HowTo {

        fun showTutorialAfterFirstTaskCreated(ctx: Activity, recyclerTask: RecyclerView) {

            if (!AppUtils.getPref(ctx, "coach_first_task_created", true))
                return

            AppUtils.putPref(ctx, "coach_first_task_created", false)

            val handler = Handler()
            handler.postDelayed({
                val card = recyclerTask.layoutManager.findViewByPosition(0)
                val viewHolder = recyclerTask.getChildViewHolder(card)
                val title = viewHolder.itemView.findViewById<TextView>(R.id.tx_titletask)

                MaterialTapTargetSequence()
                        .addPrompt(MaterialTapTargetPrompt.Builder(ctx)
                                .setTarget(title.id)
                                .setPrimaryText(ctx.getString(R.string.title_tuto_firsttask))
                                .setSecondaryText(ctx.getString(R.string.description_tuto_firsttask))
                                .setBackgroundColour(ContextCompat.getColor(ctx, R.color.black))
                                .setAutoDismiss(false)
                                .setAutoFinish(false)
                                .setPrimaryTextTypeface(ResourcesCompat.getFont(ctx, R.font.productsansbold))
                                .setSecondaryTextTypeface(ResourcesCompat.getFont(ctx, R.font.productsansregular))
                                .setCaptureTouchEventOutsidePrompt(true)
                                .setPromptStateChangeListener { prompt, state ->

                                    if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                                        prompt.finish()
                                    }
                                }
                                .create())
                        .addPrompt(MaterialTapTargetPrompt.Builder(ctx)
                                .setTarget(R.id.view2)
                                .setPrimaryText(ctx.getString(R.string.your_stats))
                                .setSecondaryText(ctx.getString(R.string.swipe_up_description))
                                .setBackgroundColour(ContextCompat.getColor(ctx, R.color.black))
                                .setAutoDismiss(false)
                                .setAutoFinish(false)
                                .setPrimaryTextTypeface(ResourcesCompat.getFont(ctx, R.font.productsansbold))
                                .setSecondaryTextTypeface(ResourcesCompat.getFont(ctx, R.font.productsansregular))
                                .setCaptureTouchEventOutsidePrompt(true)
                                .setPromptStateChangeListener { prompt, state ->

                                    if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                                        prompt.finish()
                                    }
                                }
                                .setAnimationInterpolator(LinearOutSlowInInterpolator()))

                        .show()
            }, 1000)
        }

        fun showCoachCreateCategory(ctx: Activity, id: Int) {
            if (!AppUtils.getPref(ctx, "coach_how_to_create_category", true))
                return

            AppUtils.putPref(ctx, "coach_how_to_create_category", false)

            val tutorial = showCoachMark(ctx, id, ctx.getString(R.string.custom_categories), ctx.getString(R.string.custom_categories_description))
            tutorial.setPromptBackground(RectanglePromptBackground()).promptFocal = RectanglePromptFocal()
            tutorial.show()
        }

        fun showCoachDateTitle(ctx: Activity, id: Int) {
            if (!AppUtils.getPref(ctx, "coach_how_to_change_date", true))
                return

            AppUtils.putPref(ctx, "coach_how_to_change_date", false)

            showCoachMark(ctx, id, ctx.getString(R.string.change_date), ctx.getString(R.string.tap_to_search)).show()
        }

        fun showCoachCreateTask(ctx: Activity, horizontalCalendar: HorizontalCalendar) {

            if (!AppUtils.getPref(ctx, "coach_how_to_create_first_task", true))
                return

            AppUtils.putPref(ctx, "coach_how_to_create_first_task", false)

            val handler = Handler()
            handler.postDelayed({
                val card = horizontalCalendar.calendarView.layoutManager.findViewByPosition(9)
                val viewHolder = horizontalCalendar.calendarView.getChildViewHolder(card)
                showCoachMark(ctx, viewHolder.itemView, String.format(ctx.getString(R.string.welcome),
                        AppUtils.getHello(ctx)), ctx.getString(R.string.long_tap_to_create)).show()
            }, 500)
        }

        private fun showCoachMark(context: Activity, viewId: Int, title: String, description: String): MaterialTapTargetPrompt.Builder {

            return MaterialTapTargetPrompt.Builder(context)
                    .setTarget(viewId)
                    .setPrimaryText(title)
                    .setSecondaryText(description)
                    .setBackgroundColour(ContextCompat.getColor(context, R.color.black))
                    .setAutoDismiss(false)
                    .setAutoFinish(false)
                    .setPrimaryTextTypeface(ResourcesCompat.getFont(context, R.font.productsansbold))
                    .setSecondaryTextTypeface(ResourcesCompat.getFont(context, R.font.productsansregular))
                    .setCaptureTouchEventOutsidePrompt(true)
                    .setPromptStateChangeListener { prompt, state ->

                        if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                            prompt.finish()
                        }
                    }
        }

        private fun showCoachMark(context: Activity, view: View, title: String, description: String): MaterialTapTargetPrompt.Builder {

            return MaterialTapTargetPrompt.Builder(context)
                    .setTarget(view)
                    .setPrimaryText(title)
                    .setSecondaryText(description)
                    .setBackgroundColour(ContextCompat.getColor(context, R.color.black))
                    .setAutoDismiss(false)
                    .setAutoFinish(false)
                    .setPrimaryTextTypeface(ResourcesCompat.getFont(context, R.font.productsansbold))
                    .setSecondaryTextTypeface(ResourcesCompat.getFont(context, R.font.productsansregular))
                    .setCaptureTouchEventOutsidePrompt(true)
                    .setPromptStateChangeListener { prompt, state ->

                        if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                            prompt.finish()
                        }
                    }
        }
    }
}