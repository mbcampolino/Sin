package com.codecampos.sin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codecampos.sin.R
import com.codecampos.sin.model.database.CategoryModel
import kotlinx.android.synthetic.main.item_view.view.*
import java.util.*

class CategoryAdapter(private var items: ArrayList<CategoryModel>, private val context: Context, private val listener: OnItemTaskListener) : RecyclerView.Adapter<ViewHolder>() {

    interface OnItemTaskListener {
        fun onCheck(model: CategoryModel, isChecked: Boolean)
        fun onLongClick(model: CategoryModel): Boolean
    }

    fun update(items: ArrayList<CategoryModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun remove(item: CategoryModel) {
        this.items.remove(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_view_categories, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textTitle.text = items[position].name

        holder.itemView.setOnClickListener {
            if (items[position].fixed == 0) {
                holder.checkBoxItem.isChecked = !holder.checkBoxItem.isChecked
            }
        }

        holder.checkBoxItem.isEnabled = (items[position].fixed == 0)

        if (items[position].fixed == 1) {
            holder.checkBoxItem.isChecked = true
        } else {
            holder.checkBoxItem.isChecked = items[position].checked
        }

        holder.checkBoxItem.setOnCheckedChangeListener { _, isChecked ->
            if (items[position].fixed == 0) {
                listener.onCheck(items[position], isChecked)
            }
        }

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(items[position])
        }
    }

    fun add(model: CategoryModel) {
        this.items.add(model)
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val textTitle = view.tx_titletask!!
    val checkBoxItem = view.checkbox_item!!
}