package com.codecampos.sin.adapter

import android.content.Context
import android.support.v4.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.codecampos.sin.R
import com.codecampos.sin.model.Constants
import com.codecampos.sin.model.database.CategoryModel


class SpinnerCategoryAdapter(context: Context, var listItemsTxt: ArrayList<CategoryModel>) : BaseAdapter() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.spinner_item_view, parent, false)
            vh = ItemRowHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        vh.label.text = listItemsTxt[position].name

        if (listItemsTxt[position].id_category == Constants.NEW_CATEGORY) {
            vh.label.typeface = ResourcesCompat.getFont(view.context, R.font.productsansbold)
        } else {
            vh.label.typeface = ResourcesCompat.getFont(view.context, R.font.productsansregular)
        }

        return view
    }

    override fun getItem(position: Int): CategoryModel {
        return listItemsTxt[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }

    private class ItemRowHolder(row: View?) {
        val label: TextView = row?.findViewById(R.id.tx_title) as TextView
    }
}
