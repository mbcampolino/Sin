package com.codecampos.sin.adapter

import android.content.Context
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codecampos.sin.R
import com.codecampos.sin.model.Constants.Constants.ALL_CATEGORIES
import com.codecampos.sin.model.database.CategoryModel
import com.codecampos.sin.model.database.TaskModelBeta
import kotlinx.android.synthetic.main.item_view.view.*
import java.util.*
import java.util.concurrent.TimeUnit

class TaskAdapter(var taskList: ArrayList<TaskModelBeta>, var categoryList: ArrayList<CategoryModel>, private val context: Context, private val listener: TaskListener) : RecyclerView.Adapter<ViewHolderTask>() {

    interface TaskListener {
        fun onCheck(model: TaskModelBeta, isChecked: Boolean)
        fun onLongClick(model: TaskModelBeta): Boolean
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    private fun updateCategory(position: Int, holder: ViewHolderTask) {
        var hasFindCategory = false
        for (i in 0 until categoryList.size) {
            if (categoryList[i].id_category == taskList[position].type && categoryList[i].id_category != ALL_CATEGORIES) {
                holder.textType.text = categoryList[i].name
                hasFindCategory = true
                holder.textType.visibility = View.VISIBLE
            }
        }

        if (!hasFindCategory) {
            holder.textType.visibility = View.INVISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTask {
        return ViewHolderTask(LayoutInflater.from(context).inflate(R.layout.item_view, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderTask, position: Int) {
        holder.textTitle.text = taskList[position].name
        updateCategory(position, holder)

        holder.checkItem.isChecked = taskList[position].checked
        setStroked(taskList[position].checked, holder)
        checkIsLate(taskList[position].timestamp, holder, taskList[position].checked, taskList[position].hasHour)

        holder.itemView.setOnClickListener {
            holder.checkItem.isChecked = !holder.checkItem.isChecked
        }

        holder.checkItem.setOnCheckedChangeListener { _, isChecked ->
            listener.onCheck(taskList[position], isChecked)
            setStroked(isChecked, holder)
            checkIsLate(taskList[position].timestamp, holder, isChecked, taskList[position].hasHour)
        }

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(taskList[position])
        }

        if (taskList[position].hasHour) {

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = taskList[position].timestamp

            val hourFormat: String = if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
                "0${calendar.get(Calendar.HOUR_OF_DAY)}"
            } else {
                calendar.get(Calendar.HOUR_OF_DAY).toString()
            }
            val minuteFormat: String = if (calendar.get(Calendar.MINUTE) < 10) {
                "0${calendar.get(Calendar.MINUTE)}"
            } else {
                calendar.get(Calendar.MINUTE).toString()
            }

            holder.textTime.text = "$hourFormat:$minuteFormat"
            holder.textTime.visibility = View.VISIBLE
        } else {

            holder.textTime.visibility = View.INVISIBLE
        }
    }

    fun setStroked(isChecked: Boolean, holder: ViewHolderTask) {
        if (isChecked) {
            holder.textTitle.paintFlags = holder.textTitle.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            holder.textTitle.paintFlags = holder.textTitle.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        }
    }

    fun checkIsLate(timestamp: Long, holder: ViewHolderTask, isChecked: Boolean, hasHour: Boolean) {
        if (!isChecked) {
            val calendarNow: Calendar = Calendar.getInstance()
            val calendarTask: Calendar = Calendar.getInstance()
            calendarTask.timeInMillis = timestamp

            if (hasHour) {
                if (calendarNow.timeInMillis > timestamp) {
                    holder.viewStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
                } else {
                    holder.viewStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.gray))
                }
            } else {

                val msDiff = Calendar.getInstance().timeInMillis - (calendarTask.timeInMillis)
                val daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff)
                if (daysDiff > 0) {
                    holder.viewStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
                } else {
                    holder.viewStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.gray))
                }
            }

        } else {
            holder.viewStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.gray))
        }
    }

    fun update(newCategoryList: ArrayList<CategoryModel>, taskList: ArrayList<TaskModelBeta>) {
        this.categoryList = newCategoryList
        this.taskList = taskList
        notifyDataSetChanged()
    }
}

class ViewHolderTask(view: View) : RecyclerView.ViewHolder(view) {
    val checkItem = view.checkbox_item!!
    val textTitle = view.tx_titletask!!
    val textType = view.tx_tasktype!!
    val textTime = view.tx_time!!
    val viewStatus = view.view_status!!
}

