package com.codecampos.sin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.codecampos.sin.R
import com.codecampos.sin.model.database.CategoryModel
import com.codecampos.sin.model.database.TaskModelBeta
import java.util.*


class StateBarsAdapter(private var items: ArrayList<TaskModelBeta>, private var categoryAvailableItems: ArrayList<CategoryModel>, private val context: Context) : RecyclerView.Adapter<StateBarViewHolder>() {

    override fun getItemCount(): Int {
        return categoryAvailableItems.size
    }

    fun update(itemsUpdate: ArrayList<TaskModelBeta>, categoryAvailableItemsUpdate: ArrayList<CategoryModel>) {
        items = itemsUpdate
        categoryAvailableItems = categoryAvailableItemsUpdate
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StateBarViewHolder {
        return StateBarViewHolder(LayoutInflater.from(context).inflate(R.layout.item_view_footview, parent, false))
    }

    override fun onBindViewHolder(holder: StateBarViewHolder, position: Int) {

        holder.textCategory.text = categoryAvailableItems[position].name

        val total = getValue(categoryAvailableItems[position].id_category)[0]
        val progress = getValue(categoryAvailableItems[position].id_category)[1]

        holder.progress.max = total
        holder.progress.progress = progress
        if (total > 0) {
            val percent = progress.fdiv(total).times(100)
            holder.textPercent.text = "${percent.toInt()}%"
        }
    }

    infix fun Int.fdiv(i: Int): Double = this / i.toDouble()

    fun getValue(category: Int): ArrayList<Int> {

        var total = 0
        var toCountUnchecked = 0

        for (i in 0 until items.size) {
            if (items[i].type == category) {
                total += 1
                if (!items[i].checked) {
                    toCountUnchecked += 1
                }
            }
        }

        val list = ArrayList<Int>()
        list.add(total)
        list.add(total - toCountUnchecked)
        return list
    }
}

class StateBarViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val textCategory = view.findViewById<TextView>(R.id.textView_title)!!
    val progress = view.findViewById<ProgressBar>(R.id.progress_id)!!
    val textPercent = view.findViewById<TextView>(R.id.tx_general_percent)!!
}