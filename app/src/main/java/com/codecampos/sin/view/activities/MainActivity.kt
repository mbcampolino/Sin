package com.codecampos.sin.view.activities

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomSheetBehavior
import android.support.transition.TransitionManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.codecampos.sin.CustomApplication.Companion.db
import com.codecampos.sin.R
import com.codecampos.sin.adapter.StateBarsAdapter
import com.codecampos.sin.adapter.TaskAdapter
import com.codecampos.sin.model.*
import com.codecampos.sin.model.Constants.Constants.ALL_CATEGORIES
import com.codecampos.sin.model.database.CategoryModel
import com.codecampos.sin.model.database.DateTaskModel
import com.codecampos.sin.model.database.TaskModel
import com.codecampos.sin.model.database.TaskModelBeta
import com.wang.avi.AVLoadingIndicatorView
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.HorizontalCalendarView
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import kotlinx.android.synthetic.main.foot_view.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), TaskListener, TaskAdapter.TaskListener {

    lateinit var bottomSheetDialogFragment: RegisterDialog
    lateinit var recyclerTask: RecyclerView
    lateinit var adapter: TaskAdapter
    private lateinit var horizontalCalendar: HorizontalCalendar
    lateinit var progressBarIndicador: AVLoadingIndicatorView
    lateinit var txGeneralPercent: TextView
    lateinit var frameRoot: FrameLayout

    lateinit var constraintSheet: NestedScrollView
    lateinit var toolBar: Toolbar
    lateinit var titleToolbar: TextView

    lateinit var recyclerCategory: RecyclerView
    lateinit var stateBarsAdapter: StateBarsAdapter

    lateinit var progressBarFootGeneral: ProgressBar

    private val requestAfterManageCategories = 100
    private val requestCategoriesFromRegister = 101
    private val startDelay = 1500.toLong()

    private var generalCalendar: Calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureCalendar()

        recyclerTask = findViewById(R.id.rv_list)
        recyclerCategory = findViewById(R.id.recycler_category)

        toolBar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)

        titleToolbar = findViewById(R.id.title_toolbar)
        titleToolbar.setOnClickListener(null)
        progressBarIndicador = findViewById(R.id.progress_bar)
        frameRoot = findViewById(R.id.frameLayout)
        progressBarFootGeneral = findViewById(R.id.progress_id)
        txGeneralPercent = findViewById(R.id.tx_general_percent)

        constraintSheet = findViewById(R.id.nested_bars)

        val categories = db?.taskDataDao()?.getCategoriesStatsBars() as ArrayList<CategoryModel>

        recyclerTask.layoutManager = LinearLayoutManager(this)
        recycler_category.layoutManager = LinearLayoutManager(this)

        adapter = TaskAdapter(ArrayList(), categories, this, this)

        recyclerTask.adapter = adapter

        stateBarsAdapter = StateBarsAdapter(ArrayList(), categories, baseContext)
        recyclerCategory.setHasFixedSize(true)
        recyclerCategory.isNestedScrollingEnabled = false
        recyclerCategory.adapter = stateBarsAdapter
        generalCalendar = Calendar.getInstance()

        checkPermissxionCalendar()
    }

    fun showDatePicker() {
        val c: Calendar = Calendar.getInstance()
        val picker = DatePickerDialog(this, datePicker, c
                .get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH))
        picker.show()
    }

    private fun checkPermissxionCalendar() {

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_CALENDAR) + ContextCompat
                        .checkSelfPermission(this,
                                Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_CALENDAR) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.WRITE_CALENDAR)) {
                startLoadWithDeday(startDelay, Calendar.getInstance().timeInMillis)
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR),
                        182)
            }

        } else {
            startLoadWithDeday(startDelay, Calendar.getInstance().timeInMillis)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 182) {
            if (grantResults.size > 1
                    && (grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(baseContext, getString(R.string.warning_not_granted_permission_calendar), Toast.LENGTH_LONG).show()
            } else {
                AppUtils.putPref(this, "sync_calendar", true)
            }

            startLoadWithDeday(startDelay, Calendar.getInstance().timeInMillis)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == R.id.go_to_date) {
            showDatePicker()
        }

        if (item?.itemId == R.id.manage_categories) {
            callCategories(requestAfterManageCategories)
        }

        if (item?.itemId == R.id.preferences) {
            startActivityForResult(Intent(this, PreferencesActivity::class.java), requestAfterManageCategories)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) return

        if (requestCode == requestAfterManageCategories) {
            onUpdateTasks()
        }

        if (requestCode == requestCategoriesFromRegister) {
            bottomSheetDialogFragment.populateCategory()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        horizontalCalendar.show()
        horizontalCalendar.goToday(true)
        titleToolbar.text = getString(R.string.app_name)
        generalCalendar = horizontalCalendar.selectedDate
        titleToolbar.setOnClickListener(null)

        return super.onSupportNavigateUp()
    }

    var datePicker: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

        val calendarPicker: Calendar = Calendar.getInstance()
        calendarPicker.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        calendarPicker.set(Calendar.MONTH, monthOfYear)
        calendarPicker.set(Calendar.YEAR, year)

        LoadTasks(calendarPicker.timeInMillis).execute()
        horizontalCalendar.hide()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        generalCalendar = calendarPicker

        var month: String = if (calendarPicker.get(Calendar.MONTH) < 9) {
            "0${calendarPicker.get(Calendar.MONTH).plus(1)}"
        } else {
            calendarPicker.get(Calendar.MONTH).plus(1).toString()
        }

        var day: String = if (calendarPicker.get(Calendar.DAY_OF_MONTH) < 10) {
            "0${calendarPicker.get(Calendar.DAY_OF_MONTH)}"
        } else {
            calendarPicker.get(Calendar.DAY_OF_MONTH).toString()
        }


        titleToolbar.text = ("$day/$month/${calendarPicker.get(Calendar.YEAR)}")
        titleToolbar.setOnClickListener {
            showDatePicker()
        }

        val handler = Handler()
        handler.postDelayed({
            HowTo.showCoachDateTitle(this, titleToolbar.id)
        }, 1000)
    }

    override fun onBackPressed() {

        if (horizontalCalendar.calendarView.visibility != View.VISIBLE) {
            onSupportNavigateUp()
            return
        }

        if (getBottomSheet().state == BottomSheetBehavior.STATE_EXPANDED) {
            getBottomSheet().state = BottomSheetBehavior.STATE_COLLAPSED
            return
        }

        if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) != horizontalCalendar.selectedDate.get(Calendar.DAY_OF_MONTH)) {
            horizontalCalendar.goToday(false)
            return
        }

        super.onBackPressed()
    }

    fun getBottomSheet(): BottomSheetBehavior<NestedScrollView> {
        return BottomSheetBehavior.from(constraintSheet)
    }

    private fun configureCalendar() {
        val startDate = Calendar.getInstance()
        startDate.add(Calendar.WEEK_OF_MONTH, -1)
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.WEEK_OF_MONTH, 1)

        horizontalCalendar = HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .defaultSelectedDate(Calendar.getInstance())
                .build()

        horizontalCalendar.config.isShowTopText = false

        horizontalCalendar.calendarListener = object : HorizontalCalendarListener() {

            override fun onDateSelected(date: Calendar, position: Int) {
                generalCalendar = date
                LoadTasks(date.timeInMillis).execute()
            }

            override fun onCalendarScroll(calendarView: HorizontalCalendarView?, dx: Int, dy: Int) {
                super.onCalendarScroll(calendarView, dx, dy)
            }

            override fun onDateLongClicked(date: Calendar, position: Int): Boolean {

                val calendarNow: Calendar = Calendar.getInstance()

                val days = TimeUnit.MILLISECONDS.toDays(date.timeInMillis - calendarNow.timeInMillis)

                if (days < 0) {
                    Toast.makeText(baseContext, getString(R.string.is_not_possible_old_tasks), Toast.LENGTH_SHORT).show()
                    return true
                }

                val taskModelbeta = TaskModelBeta("", 0, 0, false, 0, "", 0, 0, false, 0, 0)
                bottomSheetDialogFragment = RegisterDialog(date, taskModelbeta)
                bottomSheetDialogFragment.setListener(this@MainActivity)
                bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.tag)

                return true
            }
        }
    }

    override fun onCheck(model: TaskModelBeta, isChecked: Boolean) {
        val task = DateTaskModel(model.id_date, model.date, model.timestamp, model.id_task, isChecked)
        db?.taskDataDao()?.checkTask(isChecked, task.id_date)
        LoadStateBars().execute()
    }

    override fun onLongClick(model: TaskModelBeta): Boolean {

        bottomSheetDialogFragment = RegisterDialog(Calendar.getInstance(), model)
        bottomSheetDialogFragment.setListener(this)
        bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.tag)

        return false
    }

    internal inner class LoadTasks(var millis: Long) : AsyncTask<TaskModel, Int, ArrayList<TaskModelBeta>>() {

        lateinit var calendar: Calendar

        override fun onPreExecute() {
            super.onPreExecute()
            calendar = Calendar.getInstance()

            calendar.timeInMillis = millis
            progressBarIndicador.show()
        }

        override fun doInBackground(vararg taskModels: TaskModel): ArrayList<TaskModelBeta>? {

            val repeatList = db?.taskDataDao()?.getEveryDayEvents(AppUtils.getDate(calendar)) as ArrayList<TaskModelBeta>

            if (repeatList.size > 0) {
                for (i in 0 until repeatList.size) {

                    val count = db?.taskDataDao()?.countTaskWithId(repeatList[i].id_task, AppUtils.getDate(calendar))

                    if (count == 0) {

                        val calendarOfItem: Calendar = Calendar.getInstance()
                        calendarOfItem.timeInMillis = repeatList[i].timestampRoot

                        val newCalendarToTask: Calendar = Calendar.getInstance()
                        newCalendarToTask.timeInMillis = repeatList[i].timestamp

                        if (calendar.timeInMillis >= calendarOfItem.timeInMillis) {
                            newCalendarToTask.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH))
                            newCalendarToTask.set(Calendar.MONTH, calendar.get(Calendar.MONTH))
                            newCalendarToTask.set(Calendar.YEAR, calendar.get(Calendar.YEAR))

                            if (repeatList[i].repeatType == Constants.REPEAT_EVERYDAY) {
                                val dateTaskModel = DateTaskModel(0, AppUtils.getDate(newCalendarToTask), newCalendarToTask.timeInMillis, repeatList[i].id_task, false)
                                db?.taskDataDao()?.insertDateOfTask(dateTaskModel)
                            } else if (repeatList[i].repeatType == Constants.REPEAT_DAY_OF_MONTH) {

                                val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

                                if (calendarOfItem.get(Calendar.DAY_OF_MONTH) == dayOfMonth) {
                                    val dateTaskModel = DateTaskModel(0, AppUtils.getDate(newCalendarToTask), newCalendarToTask.timeInMillis, repeatList[i].id_task, false)
                                    db?.taskDataDao()?.insertDateOfTask(dateTaskModel)
                                }

                            } else if (repeatList[i].repeatType == Constants.REPEAT_DAY_OF_WEEK) {

                                val savedDayOfWeek = calendarOfItem.get(Calendar.DAY_OF_WEEK)
                                val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)

                                if (savedDayOfWeek == dayOfWeek) {
                                    val dateTaskModel = DateTaskModel(0, AppUtils.getDate(newCalendarToTask), newCalendarToTask.timeInMillis, repeatList[i].id_task, false)
                                    db?.taskDataDao()?.insertDateOfTask(dateTaskModel)
                                }
                            }
                        }
                    }
                }
            }

            if (ContextCompat.checkSelfPermission(baseContext, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED && AppUtils.getPref(this@MainActivity, "sync_calendar")) {
                val calendarEvents = CalendarSync.Events.getEvents(baseContext, calendar)
                if (calendarEvents.size > 0) {
                    for (i in 0 until calendarEvents.size) {
                        val calendarEvent = calendarEvents[i]
                        val calEvent: Calendar = Calendar.getInstance()
                        calEvent.timeInMillis = calendarEvent.dateRoot
                        val calendarZero = Calendar.getInstance()
                        calendarZero.set(Calendar.HOUR_OF_DAY, 0)
                        calendarZero.set(Calendar.MINUTE, 0)

                        val secondsInMilli: Long = 1000
                        val minutesInMilli = secondsInMilli * 60
                        val hoursInMilli = minutesInMilli * 60
                        val daysInMilli = hoursInMilli * 24

                        val correctDate = calEvent
                        correctDate.timeInMillis = correctDate.timeInMillis + daysInMilli
                        calendarEvent.dateRoot = correctDate.timeInMillis

                        if (correctDate.timeInMillis >= calendarZero.timeInMillis) {
                            val count = db?.taskDataDao()?.countTaskWithCalendarId(calendarEvent.fromCalendar.toInt())

                            if (count != null && count == 0) {
                                db?.taskDataDao()?.insert(calendarEvent)
                                val idOfLastTask = db?.taskDataDao()?.getLastIdFromTask() as Int
                                val calendarItem: Calendar = Calendar.getInstance()
                                calendarItem.timeInMillis = calendarEvent.dateRoot
                                val dateofTask = DateTaskModel(0, AppUtils.getDate(calendarItem), calendarItem.timeInMillis, idOfLastTask, false)
                                db?.taskDataDao()?.insertDateOfTask(dateofTask)
                            }
                        }
                    }
                }
            }

            return db?.taskDataDao()?.getFromDate(AppUtils.getDate(calendar)) as ArrayList<TaskModelBeta>
        }

        override fun onPostExecute(listLoaded: ArrayList<TaskModelBeta>) {
            super.onPostExecute(listLoaded)

            val errorHolder = findViewById<LinearLayout>(R.id.error_holder)
            TransitionManager.beginDelayedTransition(frameRoot)
            progressBarIndicador.hide()
            if (listLoaded.size > 0) {
                errorHolder.visibility = View.GONE
                recyclerTask.visibility = View.VISIBLE
            } else {
                errorHolder.visibility = View.VISIBLE
                recyclerTask.visibility = View.GONE
            }

            val categories = db?.taskDataDao()?.getCategoriesStatsBars() as ArrayList<CategoryModel>
            adapter.update(categories, listLoaded)


            if (adapter.itemCount > 0) {
                HowTo.showTutorialAfterFirstTaskCreated(this@MainActivity, recyclerTask)
            }

        }
    }

    override fun onUpdateTasks() {
        startLoadWithDeday(0, generalCalendar.timeInMillis)
    }

    fun startLoadWithDeday(delay: Long, calendarMillis: Long) {
        val handler = Handler()
        handler.postDelayed({

            if (delay == startDelay) HowTo.showCoachCreateTask(MainActivity@ this, horizontalCalendar)

            LoadStateBars().execute()
            LoadTasks(calendarMillis).execute()
        }, delay)
    }

    internal inner class LoadStateBars : AsyncTask<TaskModel, Int, ArrayList<TaskModelBeta>>() {

        var calendar: Calendar = Calendar.getInstance()

        override fun onPreExecute() {
            super.onPreExecute()
            calendar.set(Calendar.HOUR_OF_DAY, 23)
            calendar.set(Calendar.MINUTE, 59)
        }

        override fun doInBackground(vararg p0: TaskModel?): ArrayList<TaskModelBeta> {
            return db?.taskDataDao()?.getAllBeforeThatNow(calendar.timeInMillis) as ArrayList<TaskModelBeta>
        }

        override fun onPostExecute(result: ArrayList<TaskModelBeta>) {
            super.onPostExecute(result)

            TransitionManager.beginDelayedTransition(constraintSheet)

            progressBarFootGeneral.max = getValue(result, ALL_CATEGORIES)[0].toInt()
            progressBarFootGeneral.progress = getValue(result, ALL_CATEGORIES)[1].toInt()

            if (progressBarFootGeneral.max > 0) {
                val percent = progressBarFootGeneral.progress.fdiv(progressBarFootGeneral.max).times(100)
                txGeneralPercent.text = "${percent.toInt()}%"
            }

            val categories = db?.taskDataDao()?.getCategoriesStatsBars() as ArrayList<CategoryModel>

            stateBarsAdapter.update(result, categories)
        }
    }

    infix fun Int.fdiv(i: Int): Double = this / i.toDouble()

    fun getValue(result: ArrayList<TaskModelBeta>, category: Int): ArrayList<Float> {

        var total = 0
        var toCountUnchecked = 0

        for (i in 0 until result.size) {
            if (result[i].type == category || category == ALL_CATEGORIES) {
                total += 1
                if (!result[i].checked) {
                    toCountUnchecked += 1
                }
            }
        }

        val list = ArrayList<Float>()
        list.add(total.toFloat())
        list.add(total.toFloat() - toCountUnchecked.toFloat())
        return list
    }

    override fun callCategories(requestCode: Int) {
        startActivityForResult(Intent(this, ManageCategoriesActivity::class.java), requestCode)
    }
}
