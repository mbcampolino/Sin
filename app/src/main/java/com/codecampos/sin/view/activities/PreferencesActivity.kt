package com.codecampos.sin.view.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.codecampos.sin.CustomApplication.Companion.db
import com.codecampos.sin.R
import com.codecampos.sin.model.AppUtils


class PreferencesActivity : AppCompatActivity() {

    lateinit var toolBar: Toolbar
    var isEdited: Boolean = false

    lateinit var buttonSync: LinearLayout
    lateinit var checkBoxSync: CheckBox

    lateinit var buttonRate: TextView
    lateinit var buttonPrivacy: TextView
    lateinit var buttonContact: TextView
    lateinit var versionText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preferences)

        toolBar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        buttonSync = findViewById(R.id.item_sync)
        checkBoxSync = buttonSync.findViewById(R.id.checkbox_item)
        buttonRate = findViewById(R.id.tx_rate)
        buttonPrivacy = findViewById(R.id.tx_privacy)
        buttonContact = findViewById(R.id.tx_contact_developer)
        versionText = findViewById(R.id.tx_version)

        try {
            val pInfo = this.packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            versionText.text = version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        checkBoxSync.isChecked = AppUtils.getPref(this, "sync_calendar")

        buttonSync.setOnClickListener {

            if (checkBoxSync.isChecked) {
                checkBoxSync.isChecked = false
            } else {

                if (ContextCompat.checkSelfPermission(this,
                                Manifest.permission.READ_CALENDAR) + ContextCompat
                                .checkSelfPermission(this,
                                        Manifest.permission.WRITE_CALENDAR)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR),
                            182)

                } else {
                    checkBoxSync.isChecked = true
                }
            }
        }

        buttonRate.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id=com.codecampos.sin")
            startActivity(intent)
        }

        buttonContact.setOnClickListener {
            val emailIntent = Intent(android.content.Intent.ACTION_SEND)
            emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            emailIntent.type = "vnd.android.cursor.item/email"
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, arrayOf("mbcampolino@gmail.com"))
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Support Sin App")
            startActivity(Intent.createChooser(emailIntent, "Send mail using..."))
        }

        buttonPrivacy.setOnClickListener {
            val url = "https://github.com/mbcampolino/Sin/blob/master/Mobile_Privacy_Policy.pdf"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        checkBoxSync.setOnCheckedChangeListener { _, isChecked ->

            AppUtils.putPref(this, "sync_calendar", isChecked)

            val count = db?.taskDataDao()?.countTaskWithCalendar()
            if (count != null) {
                if (!isChecked && count > 0) {
                    val alert = AlertDialog.Builder(this)
                    alert.setMessage("Existem tarefas sincronizadas do calendario. Se você desativar irá remover elas do app. Deseja desativar mesmo assim?")
                    alert.setNegativeButton("desativar", { _, _ ->
                        db?.taskDataDao()?.removeTasksWithCalendar()
                        isEdited = true
                    })
                    alert.setPositiveButton("manter", { dialogInterface, _ ->
                        dialogInterface.cancel()
                        checkBoxSync.isChecked = true
                    })
                    alert.setCancelable(false)
                    alert.create()
                    alert.show()
                }
            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 182) {
            if (grantResults.isNotEmpty()
                    && grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(baseContext, getString(R.string.warning_not_granted_permission_calendar), Toast.LENGTH_LONG).show()
                checkBoxSync.isChecked = false
                return
            }

            checkBoxSync.isChecked = true
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {

        val intentBack = Intent()
        if (isEdited) setResult(Activity.RESULT_OK, intentBack)

        finish()
    }
}
