package com.codecampos.sin.view.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.transition.TransitionManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.TextView.OnEditorActionListener
import com.codecampos.sin.CustomApplication.Companion.db
import com.codecampos.sin.R
import com.codecampos.sin.adapter.CategoryAdapter
import com.codecampos.sin.model.HowTo
import com.codecampos.sin.model.database.CategoryModel
import com.rengwuxian.materialedittext.MaterialEditText

class ManageCategoriesActivity : AppCompatActivity(), CategoryAdapter.OnItemTaskListener {

    lateinit var recyclerTask: RecyclerView
    lateinit var adapter: CategoryAdapter
    lateinit var toolBar: Toolbar
    lateinit var progressBar: ProgressBar
    lateinit var edCategory: MaterialEditText
    lateinit var frameLayout: FrameLayout

    lateinit var bottomSheet: LinearLayout

    var isEdited = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_category)

        recyclerTask = findViewById(R.id.rv_list)

        progressBar = findViewById(R.id.progress_bar)
        edCategory = findViewById(R.id.ed_category_name)
        frameLayout = findViewById(R.id.frameLayout)
        bottomSheet = findViewById(R.id.linear_category_foot)
        getBottomSheet().state = BottomSheetBehavior.STATE_HIDDEN

        toolBar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        recyclerTask.layoutManager = LinearLayoutManager(this)
        recyclerTask.setHasFixedSize(true)
        recyclerTask.isNestedScrollingEnabled = false
        adapter = CategoryAdapter(ArrayList(), this, this)
        recyclerTask.adapter = adapter

        LoadCategories().execute()

        edCategory.setOnEditorActionListener(OnEditorActionListener { _, _, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {

                if (adapter.itemCount >= 20) {
                    Toast.makeText(baseContext, getString(R.string.limite_category), Toast.LENGTH_SHORT).show()
                } else if (edCategory.text.isNotEmpty()) {
                    val category = CategoryModel(0, edCategory.text.toString(), true, 0)
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, InputMethodManager.SHOW_FORCED)
                    SaveCategory(category).execute()
                    edCategory.text.clear()
                    LoadCategories().execute()
                    isEdited = true
                    return@OnEditorActionListener true
                }
            }
            false
        })

        HowTo.showCoachCreateCategory(this, edCategory.id)
    }

    fun getBottomSheet(): BottomSheetBehavior<LinearLayout> {
        return BottomSheetBehavior.from(bottomSheet)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {

        if (getBottomSheet().state == BottomSheetBehavior.STATE_EXPANDED) {
            getBottomSheet().state = BottomSheetBehavior.STATE_HIDDEN
            return
        }

        val intentBack = Intent()
        if (isEdited) setResult(Activity.RESULT_OK, intentBack)

        finish()
    }

    internal inner class LoadCategories : AsyncTask<CategoryModel, Int, ArrayList<CategoryModel>>() {

        override fun onPreExecute() {
            super.onPreExecute()
            progressBar.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg p0: CategoryModel?): ArrayList<CategoryModel> {
            return db?.taskDataDao()?.getCategories() as ArrayList<CategoryModel>
        }

        override fun onPostExecute(result: ArrayList<CategoryModel>) {
            super.onPostExecute(result)
            TransitionManager.beginDelayedTransition(frameLayout)
            progressBar.visibility = View.GONE
            adapter.update(result)
        }
    }

    class SaveCategory(var categoryModel: CategoryModel) : AsyncTask<Unit?, Unit?, Unit?>() {
        override fun doInBackground(vararg p0: Unit?): Unit? {
            return db?.taskDataDao()?.insert(categoryModel)
        }
    }

    override fun onCheck(model: CategoryModel, isChecked: Boolean) {
        db?.taskDataDao()?.checkCategory(isChecked, model.id_category)
        isEdited = true
    }

    override fun onLongClick(model: CategoryModel): Boolean {

        getBottomSheet().state = BottomSheetBehavior.STATE_EXPANDED

        val editText = bottomSheet.findViewById(R.id.ed_category_name) as MaterialEditText
        val btnSave = bottomSheet.findViewById(R.id.btn_save) as TextView
        val btnRemove = bottomSheet.findViewById(R.id.btn_remove) as TextView
        editText.setText(model.name)

        btnRemove.setOnClickListener {
            db?.taskDataDao()?.delete(model)
            adapter.remove(model)
            adapter.notifyDataSetChanged()
            getBottomSheet().state = BottomSheetBehavior.STATE_HIDDEN
            db?.taskDataDao()?.updateTasksWithCategory(model.id_category)
            isEdited = true
        }

        btnSave.setOnClickListener {
            db?.taskDataDao()?.changeCategoryName(editText.text.toString(), model.id_category)
            model.name = editText.text.toString()
            adapter.remove(model)
            adapter.add(model)
            adapter.notifyDataSetChanged()
            getBottomSheet().state = BottomSheetBehavior.STATE_HIDDEN
            isEdited = true
        }

        return true
    }
}
