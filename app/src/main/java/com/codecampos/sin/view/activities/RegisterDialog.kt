package com.codecampos.sin.view.activities

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.provider.CalendarContract
import android.support.design.widget.BottomSheetDialogFragment
import android.support.transition.TransitionManager
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import com.codecampos.sin.CustomApplication
import com.codecampos.sin.CustomApplication.Companion.db
import com.codecampos.sin.R
import com.codecampos.sin.adapter.SpinnerAdapter
import com.codecampos.sin.adapter.SpinnerCategoryAdapter
import com.codecampos.sin.model.AppUtils
import com.codecampos.sin.model.Constants.Constants.ALL_CATEGORIES
import com.codecampos.sin.model.Constants.Constants.NEW_CATEGORY
import com.codecampos.sin.model.TaskListener
import com.codecampos.sin.model.database.CategoryModel
import com.codecampos.sin.model.database.DateTaskModel
import com.codecampos.sin.model.database.TaskModel
import com.codecampos.sin.model.database.TaskModelBeta
import com.rengwuxian.materialedittext.MaterialEditText
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("ValidFragment")
class RegisterDialog(var date: Calendar, private var taskModel: TaskModelBeta) : BottomSheetDialogFragment() {

    lateinit var edName: MaterialEditText
    lateinit var edWhen: EditText
    lateinit var edHour: EditText
    lateinit var spRepeat: Spinner
    lateinit var spCategory: Spinner
    lateinit var btnSave: TextView
    lateinit var btnDelete: TextView
    lateinit var warningSpinnerRepeat: TextView
    lateinit var checkHasHour: CheckBox

    lateinit var calendarEdited: Calendar
    lateinit var linearRoot: LinearLayout
    lateinit var spinnerArrayAdapter: SpinnerCategoryAdapter

    var editable: Boolean = false

    lateinit var taskListener: TaskListener

    override fun setupDialog(dialog: Dialog?, style: Int) {
        val contentView = View.inflate(context, R.layout.dialog_register, null)
        dialog?.setContentView(contentView)
        val bottomSheet = dialog?.window?.findViewById(android.support.design.R.id.design_bottom_sheet) as FrameLayout
        bottomSheet.setBackgroundResource(R.drawable.foot_background)

        linearRoot = contentView.findViewById(R.id.linear)
        edName = contentView.findViewById(R.id.ed_task_name)
        warningSpinnerRepeat = contentView.findViewById(R.id.warning_spinner_repeat)
        edWhen = contentView.findViewById(R.id.ed_date)
        edWhen.inputType = 0
        edHour = contentView.findViewById(R.id.ed_time)
        edHour.inputType = 0
        spRepeat = contentView.findViewById(R.id.sp_repeat)
        spCategory = contentView.findViewById(R.id.sp_category)
        btnSave = contentView.findViewById(R.id.btn_save)
        btnDelete = contentView.findViewById(R.id.btn_remove)
        checkHasHour = contentView.findViewById(R.id.checkBox_has_hour)

        val c = Calendar.getInstance()
        calendarEdited = c

        if (taskModel.id_task != 0) {
            editable = true
            btnDelete.visibility = View.VISIBLE

            if (taskModel.fromCalendar > 0) {
                btnDelete.text = "remove from calendar"
            }
            edName.setText(taskModel.name)
            edWhen.setText(taskModel.date)
            checkHasHour.isChecked = taskModel.hasHour

            if (taskModel.hasHour) {
                val cal = Calendar.getInstance()
                cal.timeInMillis = taskModel.timestamp

                var hourFormat: String = if (cal.get(Calendar.HOUR_OF_DAY) < 10) {
                    "0${cal.get(Calendar.HOUR_OF_DAY)}"
                } else {
                    cal.get(Calendar.HOUR_OF_DAY).toString()
                }
                var minuteFormat: String = if (cal.get(Calendar.MINUTE) < 10) {
                    "0${cal.get(Calendar.MINUTE)}"
                } else {
                    cal.get(Calendar.MINUTE).toString()
                }

                edHour.setText("$hourFormat:$minuteFormat")
                edHour.visibility = View.VISIBLE
            }

        } else {
            calendarEdited = date

            var month: String = if (calendarEdited.get(Calendar.MONTH) < 9) {
                "0${calendarEdited.get(Calendar.MONTH).plus(1)}"
            } else {
                calendarEdited.get(Calendar.MONTH).plus(1).toString()
            }

            var day: String = if (calendarEdited.get(Calendar.DAY_OF_MONTH) < 10) {
                "0${calendarEdited.get(Calendar.DAY_OF_MONTH)}"
            } else {
                calendarEdited.get(Calendar.DAY_OF_MONTH).toString()
            }


            edWhen.setText("$day/$month/${calendarEdited.get(Calendar.YEAR)}")
        }

        edWhen.setOnClickListener {
            val picker = DatePickerDialog(activity, datePicker, c
                    .get(Calendar.YEAR), c.get(Calendar.MONTH),
                    c.get(Calendar.DAY_OF_MONTH))
            picker.datePicker.minDate = Calendar.getInstance().timeInMillis - 1000
            picker.show()
        }

        edHour.setOnClickListener {
            TimePickerDialog(activity, time, c
                    .get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE),
                    true).show()
        }

        checkHasHour.setOnCheckedChangeListener({ _, isChecked ->
            TransitionManager.beginDelayedTransition(linearRoot)
            edHour.visibility = if (isChecked) View.VISIBLE else View.GONE
            updateWhen()
        })

        populateCategory()

        updateWhen()

        spRepeat.setSelection(taskModel.repeatType)

        btnDelete.setOnClickListener {

            if (taskModel.fromCalendar > 0) {
                var deleteUri: Uri? = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, taskModel.fromCalendar)
                val rows = activity?.contentResolver?.delete(deleteUri, null, null)
                if (rows != null) {
                    if (rows > 0) {
                        val task = TaskModel(taskModel.id_task, edName.text.toString(), spinnerArrayAdapter.getItem(spCategory.selectedItemPosition).id_category, !edHour.text.isEmpty() && checkHasHour.isChecked, spRepeat.selectedItemPosition, taskModel.timestampRoot, taskModel.fromCalendar)

                        db?.taskDataDao()?.deleteDatesOfTask(taskModel.id_task)
                        db?.taskDataDao()?.delete(task)

                        taskListener.onUpdateTasks()
                        getDialog().dismiss()
                    } else {
                        Toast.makeText(activity, "Houve um problema ao remover a tarefa do calendario.", Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                val task = TaskModel(taskModel.id_task, edName.text.toString(), spinnerArrayAdapter.getItem(spCategory.selectedItemPosition).id_category, !edHour.text.isEmpty() && checkHasHour.isChecked, spRepeat.selectedItemPosition, taskModel.timestampRoot, taskModel.fromCalendar)
                db?.taskDataDao()?.deleteDatesOfTask(taskModel.id_task)
                db?.taskDataDao()?.delete(task)
                taskListener.onUpdateTasks()
                getDialog().dismiss()
            }


        }

        btnSave.setOnClickListener {
            val timestamp = calendarEdited.timeInMillis
            Log.v("DATE TO TIMEMILLIS", timestamp.toString())

            val task = TaskModel(taskModel.id_task, edName.text.toString(), spinnerArrayAdapter.getItem(spCategory.selectedItemPosition).id_category, !edHour.text.isEmpty() && checkHasHour.isChecked, spRepeat.selectedItemPosition, calendarEdited.timeInMillis, taskModel.fromCalendar)
            val date = DateTaskModel(taskModel.id_date, edWhen.text.toString(), calendarEdited.timeInMillis, taskModel.id_task, taskModel.checked)

            if (edName.text.isNotEmpty()) {

                if (editable) {
                    db?.taskDataDao()?.update(task)
                    db?.taskDataDao()?.updateDate(date)
                } else {
                    // insert a new task
                    db?.taskDataDao()?.insert(task)
                    // return id from task
                    val idOfLastTask = db?.taskDataDao()?.getLastIdFromTask() as Int
                    // insert dateoftask
                    val dateofTask = DateTaskModel(0, edWhen.text.toString(), calendarEdited.timeInMillis, idOfLastTask, false)
                    db?.taskDataDao()?.insertDateOfTask(dateofTask)
                }

                taskListener.onUpdateTasks()
                getDialog().dismiss()
            } else {
                edName.requestFocus()
                edName.error = getString(R.string.put_a_name_of_task)
            }
        }

        spRepeat.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                // your code here
                if (position > 0) {

                    if (calendarEdited.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        return

                    if (!AppUtils.getPref(selectedItemView.context, "warning_isshow")) {
                        AppUtils.putPref(selectedItemView.context, "warning_isshow", true)
                        TransitionManager.beginDelayedTransition(linearRoot)
                        warningSpinnerRepeat.visibility = View.VISIBLE
                        warningSpinnerRepeat.setOnClickListener {
                            warningSpinnerRepeat.visibility = View.GONE
                        }
                    }
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }

        }
    }

    var time: TimePickerDialog.OnTimeSetListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->

        var hourFormat: String = if (hourOfDay < 10) {
            "0$hourOfDay"
        } else {
            hourOfDay.toString()
        }
        var minuteFormat: String = if (minute < 10) {
            "0$minute"
        } else {
            minute.toString()
        }

        edHour.setText("$hourFormat:$minuteFormat")
        updateWhen()
    }

    var datePicker: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

        var month: String = if (monthOfYear < 9) {
            "0${monthOfYear.plus(1)}"
        } else {
            monthOfYear.plus(1).toString()
        }

        var day: String = if (dayOfMonth < 10) {
            "0$dayOfMonth"
        } else {
            dayOfMonth.toString()
        }

        edWhen.setText("$day/$month/$year")
        updateWhen()

    }

    private fun updateWhen(hasDay: Boolean = !edWhen.text.isEmpty(), hasHour: Boolean = !edHour.text.isEmpty()) {
        var sdf: SimpleDateFormat?

        if (hasDay && hasHour) {
            sdf = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
            calendarEdited.time = sdf.parse(edWhen.text.toString() + " " + edHour.text.toString())
        } else if (hasDay) {
            sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            calendarEdited.time = sdf.parse(edWhen.text.toString())
        }

        populateSpinner(AppUtils.getDayOfWeekName(calendarEdited.get(Calendar.DAY_OF_WEEK), resources), calendarEdited.get(Calendar.DAY_OF_MONTH))
    }

    fun populateCategory() {
        class LoadCategories(val context: Context) : AsyncTask<CategoryModel, Int, ArrayList<CategoryModel>>() {
            override fun doInBackground(vararg p0: CategoryModel?): ArrayList<CategoryModel> {
                return CustomApplication.db?.taskDataDao()?.getCategoriesStatsBars(0) as ArrayList<CategoryModel>
            }

            override fun onPostExecute(result: ArrayList<CategoryModel>) {
                super.onPostExecute(result)
                result.add(0, CategoryModel(ALL_CATEGORIES, context.getString(R.string.nothing), true, 0))
                result.add(CategoryModel(NEW_CATEGORY, context.getString(R.string.manage_categories), true, 0))
                spinnerArrayAdapter = SpinnerCategoryAdapter(context, result)
                spCategory.adapter = spinnerArrayAdapter

                spCategory.setSelection(getCategoryPos(result))

                spCategory.onItemSelectedListener = object : OnItemSelectedListener {
                    override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                        // your code here
                        if (position == spinnerArrayAdapter.count - 1) {
                            spCategory.setSelection(getCategoryPos(result))
                            taskListener.callCategories(101)
                        }
                    }

                    override fun onNothingSelected(parentView: AdapterView<*>) {
                        // your code here
                    }

                }
            }
        }
        LoadCategories(activity!!).execute()
    }

    fun getCategoryPos(result: ArrayList<CategoryModel>): Int {
        for (i in 0 until result.size) {
            if (result[i].id_category == taskModel.type) {
                return i
            }
        }
        return 0
    }

    fun populateSpinner(dayOfWeek: String, dayOfMonth: Int) {

        if (spRepeat.adapter != null && spRepeat.adapter.count > 0) return

        val array = ArrayList<String>()

        array.add(getString(R.string.once))
        array.add(getString(R.string.everyday))
        array.add(String.format(getString(R.string.always_on), dayOfWeek))
        array.add(String.format(getString(R.string.everyday_on), dayOfMonth))

        val spinnerArrayAdapter = SpinnerAdapter(this.context!!, array)
        spRepeat.adapter = spinnerArrayAdapter
    }

    fun setListener(taskListener: TaskListener) {
        this.taskListener = taskListener
    }
}
