package com.codecampos.sin

import android.app.Application
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.codecampos.sin.model.AppUtils
import com.codecampos.sin.model.database.AppDatabase
import java.util.concurrent.Executors


class CustomApplication : Application() {

    companion object {
        var db: AppDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()

        db = Room.databaseBuilder(applicationContext,
                AppDatabase::class.java, "tasks.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Executors.newSingleThreadScheduledExecutor().execute({
                            Companion.db?.taskDataDao()?.insertAll(AppUtils.getMockedCategories(resources))
                        })
                    }
                })
                .build()


    }

}